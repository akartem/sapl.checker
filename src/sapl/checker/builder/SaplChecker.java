package sapl.checker.builder;

import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;

import sapl.core.sapln3.SaplCode;
import sapl.core.sapln3.SaplN3Parser;
import sapl.core.sapln3.SaplSyntaxError;
import sapl.core.sapln3.Token;

/**
 * @author Artem Katasonov (VTT)
 * 
 * @version 1.6 (07.02.2018)
 * 
 * Copyright (C) 2014-2018, VTT
 * 
 * */

public class SaplChecker extends IncrementalProjectBuilder {

	public static final String BUILDER_ID = "sapl.checker.saplChecker";
	private static final String MARKER_TYPE = "sapl.checker.saplProblem";
	
	HashSet<IFile> filesWithMarkers = new HashSet<IFile>();

	class SaplDeltaVisitor implements IResourceDeltaVisitor {

		public boolean visit(IResourceDelta delta) throws CoreException {
			IResource resource = delta.getResource();
			switch (delta.getKind()) {
			case IResourceDelta.ADDED:
				checkSaplFile(resource);
				break;
			case IResourceDelta.REMOVED:
				if (resource instanceof IFile && (resource.getName().endsWith(".sapl") || resource.getName().endsWith(".ttl")))
					deleteMarkers((IFile)resource);
				break;
			case IResourceDelta.CHANGED:
				checkSaplFile(resource);
				break;
			}
			//return true to continue visiting children.
			return true;
		}
	}

	class SaplResourceVisitor implements IResourceVisitor {
		public boolean visit(IResource resource) {
			checkSaplFile(resource);
			//return true to continue visiting children.
			return true;
		}
	}


	@Override
	protected void clean(IProgressMonitor monitor) throws CoreException {
		getProject().deleteMarkers(MARKER_TYPE, true, IResource.DEPTH_INFINITE);
		SaplCodeVerifier.clean();
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected IProject[] build(int kind, Map args, IProgressMonitor monitor)
			throws CoreException {
		
		if (kind == FULL_BUILD) {
			fullBuild(monitor);
		} else {
			IResourceDelta delta = getDelta(getProject());
			if (delta == null) {
				fullBuild(monitor);
			} else {
				incrementalBuild(delta, monitor);
			}
		}
		return null;
	}

	protected void fullBuild(final IProgressMonitor monitor)
			throws CoreException {
		try {
			getProject().accept(new SaplResourceVisitor());

			//do second pass on files with errors/warnings
			HashSet<IFile> toRecheck = new HashSet<IFile>(filesWithMarkers);
			for(IFile resource : toRecheck)
				checkSaplFile(resource);
			
		} catch (CoreException e) {
		}
	}

	protected void incrementalBuild(IResourceDelta delta,
			IProgressMonitor monitor) throws CoreException {
		delta.accept(new SaplDeltaVisitor());
	}

	private void addMarker(IFile file, String message, Token token, int severity) {
		try {
			
			IMarker marker = file.createMarker(MARKER_TYPE);
			marker.setAttribute(IMarker.MESSAGE, message);
			marker.setAttribute(IMarker.SEVERITY, severity);
			if (token != null) {
				marker.setAttribute(IMarker.LINE_NUMBER, token.getLine());
				marker.setAttribute(IMarker.CHAR_START, token.getTextCharStart());
				marker.setAttribute(IMarker.CHAR_END, token.getTextCharStart()+token.getToken().length());
			}
			else
				marker.setAttribute(IMarker.LINE_NUMBER, 1);
		} catch (CoreException e) {
		}
	}

	private void deleteMarkers(IFile file) {
		try {
			file.deleteMarkers(MARKER_TYPE, false, IResource.DEPTH_ZERO);
		} catch (CoreException ce) {
		}
	}


	/**Compile and report errors in a file*/
	void checkSaplFile(IResource resource) {
		if (resource instanceof IFile && (resource.getName().endsWith(".sapl") || resource.getName().endsWith(".ttl"))) {
			IFile file = (IFile) resource;
			deleteMarkers(file);
			filesWithMarkers.remove(file);
			
			String filename = file.getLocation().toString();
			//System.out.println("Checking "+filename);

			try {
				SaplCode saplCode = SaplN3Parser.compileFile(filename);
				
				if(saplCode!=null){
					List<SaplIssue> issues = SaplCodeVerifier.verify(saplCode);
					for(SaplIssue issue : issues){
						int severity = issue.getSeverity();
						if(severity==SaplIssue.SEVERITY_ERROR) severity = IMarker.SEVERITY_ERROR;
						else severity = IMarker.SEVERITY_WARNING;
						addMarker(file, issue.getMessage(), issue.getToken(), severity);
					}
					if(issues.size()>0) filesWithMarkers.add(file);
				}
				
//				SaplModel model = new SaplModel(false, false);
//				SaplN3Parser.load(code.getCode(), model, SaplConstants.GENERAL_CONTEXT, filename, false);
			} catch (SaplSyntaxError er) {
				addMarker(file, er.getMessage(), er.getToken(), IMarker.SEVERITY_ERROR);
			}
			catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

}
