package sapl.checker.builder;

import sapl.core.sapln3.Token;

/**
 * @author Artem Katasonov (VTT)
 * 
 * @version 1.0 (10.10.2014)
 * 
 * Copyright (C) 2014, VTT
 * 
 * */

public class SaplIssue{

	public static int SEVERITY_ERROR = 1;
	public static int SEVERITY_WARNING = 2;

	String message;
	int severity;
	String textID;
	Token token;
	
	public SaplIssue(String message, int severity, String textID, Token token){
		this.message = message;
		this.severity = severity;
		this.token = token;
		this.textID = textID;
	}
	
	public String getTextID(){
		return textID;
	}

	public Token getToken(){
		return token;
	}
	
	public String getMessage(){
		return message;
	}

	public int getSeverity(){
		return severity;
	}

}
