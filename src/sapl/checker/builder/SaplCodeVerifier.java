package sapl.checker.builder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import org.eclipse.core.runtime.Platform;
import org.osgi.framework.Bundle;

import sapl.core.ExpressionEvaluator;
import sapl.core.SaplConstants;
import sapl.core.SaplModel;
import sapl.core.SaplQueryEngine;
import sapl.core.SaplQueryResultSet;
import sapl.core.sapln3.SaplCode;
import sapl.core.sapln3.SaplN3Parser;
import sapl.core.sapln3.Token;
import sapl.shared.eii.EiiConstants;

/**
 * @author Artem Katasonov (VTT)
 * 
 * @version 1.1 (20.10.2015)
 * 
 * Copyright (C) 2014-2015, VTT
 * 
 * */

public class SaplCodeVerifier {
	
	static final String RDFS_RESOURCE = SaplConstants.RDFS_NS+"Resource";
	static final String RDF_PROPERTY = SaplConstants.RDF_NS+"Property";
	static final String RDFS_CLASS = SaplConstants.RDFS_NS+"Class";
	static final String XSD_INTEGER = SaplConstants.XSD_NS+"integer";
	static final String XSD_BOOLEAN = SaplConstants.XSD_NS+"boolean";
	static final String XSD_STRING = SaplConstants.XSD_NS+"string";
	
	static final String SAPL_EXTRA_NS = "http://www.ubiware.jyu.fi/sapl_upper#";
	static final String FUNCTION = SAPL_EXTRA_NS+"Function";
	static final String BEHAVIOR = SAPL_EXTRA_NS+"Behavior";
	static final String PARAMETER = SAPL_EXTRA_NS+"Parameter";
	static final String SINGLE_PROPERTY_CLASS = SAPL_EXTRA_NS+"SinglePropertyClass";
	static final String SINGLE_PROPERTY_VALUE = SAPL_EXTRA_NS+"SinglePropertyValue";

	static final String SAPL_NS_N3 = '<'+SaplConstants.SAPL_NS+'>';
	static final String RAB_NS_N3 = '<'+SaplConstants.RAB_NS+'>';
	static final String META_NS_N3 = '<'+SaplConstants.META_NS+'>';
	static final String ONTONUT_NS_N3 = '<'+SaplConstants.ONTONUT_NS+'>';
	static final String DATA_NS_N3 = '<'+SaplConstants.DATA_NS+'>';
	static final String PARAM_NS_N3 = '<'+SaplConstants.PARAM_NS+'>';
	static final String RDF_NS_N3 = '<'+SaplConstants.RDF_NS+'>';
	static final String RDFS_NS_N3 = '<'+SaplConstants.RDFS_NS+'>';
	static final String OWL_NS_N3 = '<'+SaplConstants.OWL_NS+'>';
	static final String XSD_NS_N3 = '<'+SaplConstants.XSD_NS+'>';
	
	static final int QUERY_NATURE_RULE_HEAD = 0;
	static final int QUERY_NATURE_REMOVE = 1;
	static final int QUERY_NATURE_ONTONUT_PARAMS = 2;
	
	static Map<String,String> report = new HashMap<String, String>();
	static{
		report.put(BEHAVIOR, "a Reusable Atomic Behavior or a custom procedure (or a variable)");
		report.put(SaplConstants.Variable, "a variable");
		report.put(XSD_INTEGER, "an integer number");
		report.put(XSD_STRING, "a string");
		report.put(XSD_BOOLEAN, "a true/false value");
		report.put(PARAMETER, "a parameter");
		report.put(SaplConstants.Context, "a context container");
	}
	
	static Map<String,List<String>> globalCustomConcepts = new HashMap<String,List<String>>();
	
	public static void clean(){
		synchronized (globalCustomConcepts) {
			globalCustomConcepts.clear();
		}
	}
	
	/**Verify various things*/
	public static List<SaplIssue> verify(SaplCode code){
		List<SaplIssue> issues = new ArrayList<SaplIssue>();
		
		List<SaplIssue> saplConceptIssues = verifySaplConcepts(code);
		if(saplConceptIssues!=null)	issues.addAll(saplConceptIssues);

		List<SaplIssue> customConceptsIssues = verifyCustomConceptsAndVars(code);
		if(customConceptsIssues!=null)	issues.addAll(customConceptsIssues);
		
		return issues;
	}

	/**Any concept in S-APL namespace should be registered SaplConstants.concepts,
	 * otherwise a typo.
	 * Also, verify domains, ranges with respect to S-APL ontology definitions*/
	private static List<SaplIssue> verifySaplConcepts(SaplCode code){
		String saplPrefix = "s:"; 
		String ontoPrefix = null; 
		String paramPrefix = null; 
		List<SaplIssue> issues = null;
		List<Token> tokens = code.getTokens();
		for(int i=0; i<tokens.size(); i++){
			Token t = tokens.get(i);
			String token = t.getToken();
			if(token.equals(SaplN3Parser.PREFIX_KEYWORD) && i+2<tokens.size()){
				if(tokens.get(i+2).getToken().equals(SAPL_NS_N3)){
					saplPrefix = tokens.get(i+1).getToken();
					i=i+2;
				}
				if(tokens.get(i+2).getToken().equals(ONTONUT_NS_N3)){
					ontoPrefix = tokens.get(i+1).getToken();
					i=i+2;
				}
				else if(tokens.get(i+2).getToken().equals(PARAM_NS_N3)){
					paramPrefix = tokens.get(i+1).getToken();
					i=i+2;
				}
			}
			String concept = null;
			boolean isSaplConcept = false;
			String replace = SaplN3Parser.replace.get(token);
			if(replace!=null){
				if(replace.startsWith(SaplConstants.SAPL_NS)) concept = replace;
			}
			else if(saplPrefix!=null && token.length() > saplPrefix.length() && token.startsWith(saplPrefix)){
				String keyword = token.substring(saplPrefix.length());
				concept = SaplConstants.SAPL_NS+keyword;
				isSaplConcept = true;
			}
			else if(ontoPrefix!=null && token.length() > ontoPrefix.length() && token.startsWith(ontoPrefix)){
				String keyword = token.substring(ontoPrefix.length());
				concept = SaplConstants.ONTONUT_NS+keyword;
			}
			
			if(concept!=null){
				if(isSaplConcept && !SaplConstants.concepts.contains(concept)){
					if(issues==null) issues = new ArrayList<SaplIssue>(); 
					issues.add(new SaplIssue("Token '"+token+"' is not a valid S-APL concept", SaplIssue.SEVERITY_ERROR, code.getTextID(), t));
				}
				else{
					String tokenAfter = getTokenAfter(tokens, i);
					tokenAfter = fixToken(tokenAfter, saplPrefix, paramPrefix);

					String tokenBefore = getTokenBefore(tokens, i);
					tokenBefore = fixToken(tokenBefore, saplPrefix, paramPrefix);
					
					if(!tokenAfter.equals(SaplConstants.rdfType)){
						List<String> conceptIssues = verifyOntologyConcept(concept, tokenBefore, tokenAfter);
						if(conceptIssues != null){
							if(issues==null) issues = new ArrayList<SaplIssue>(); 
							for(String conceptIssue : conceptIssues){
								String prefixed = conceptIssue.replace(SaplConstants.SAPL_NS, saplPrefix);
								if(ontoPrefix!=null) prefixed = prefixed.replace(SaplConstants.ONTONUT_NS, ontoPrefix);
								issues.add(new SaplIssue(prefixed, SaplIssue.SEVERITY_ERROR, code.getTextID(), t));
							}
						}
					}
				}
			}
		}
		return issues;
	}
	
	private static String getTokenBefore(List<Token> tokens, int i){
		if(i>0) return tokens.get(i-1).getToken();
		else return "<Start of File>";
	}
	
	private static String getTokenAfter(List<Token> tokens, int i){
		if(i < tokens.size()-1)  return tokens.get(i+1).getToken();
		else return "<End of File>";
	}
	
	private static String fixToken(String token, String saplPrefix, String paramPrefix){
		String replace = SaplN3Parser.replace.get(token);
		if(replace!=null){
			token = replace;
		}
		else{
			if(token.startsWith(saplPrefix)) token = SaplConstants.SAPL_NS+token.substring(saplPrefix.length());
			else if(paramPrefix!=null && token.startsWith(paramPrefix)) token = SaplConstants.PARAM_NS+token.substring(paramPrefix.length());
		}		
		return token;
	}
	
	private static List<String> verifyOntologyConcept(String concept, String tokenBefore, String tokenAfter) {
		List<String> issues = null;
		String domain = domains.get(concept);
		String range = ranges.get(concept);
		List<String> cls = individuals.get(concept);
		if(classes.contains(concept) || cls!=null){
			boolean asSubject = tokenBefore.equals(".") || tokenBefore.equals("{");
			boolean asObject = tokenAfter.equals(".") || tokenAfter.equals(";") || tokenAfter.equals(",") || tokenAfter.equals("]") || tokenAfter.equals("}") || tokenAfter.equals("<End of File>");
			if(!asSubject && !asObject){
				if(issues==null) issues = new ArrayList<String>(2);
				issues.add(concept+" cannot be used as a predicate in a statement.");				
			}
			else{
				List<String> supers = superclasses.get(concept);
				if(asSubject){
					if(!SaplConstants.log_predicates.contains(tokenAfter)){
						Set<String> acceptable = new HashSet<String>();
						List<String> own = propertiesOfSubject.get(concept);
						if(own!=null) acceptable.addAll(own);
						if(cls!=null){
							for(String cl : cls){
								List<String> ofClass = propertiesOfSubject.get(cl);
								if(ofClass!=null) acceptable.addAll(ofClass);
							}
						}
						if(supers!=null){
							for(String cl : supers){
								List<String> ofClass = propertiesOfSubject.get(cl);
								if(ofClass!=null) acceptable.addAll(ofClass);
							}
						}
						if(!acceptable.isEmpty()){
							if(!acceptable.contains(tokenAfter)){
								if(issues==null) issues = new ArrayList<String>(1);
								if(acceptable.size()==1)
									issues.add(concept+" as a subject must be in a statement with "+acceptable.iterator().next()+" as the predicate. \nInstead, '"+tokenAfter+"' is found.");
								else issues.add(concept+" as a subject must be in a statement with the predicate that is one of "+acceptable.toString()+". \nInstead, '"+tokenAfter+"' is found.");				
							}
						}
					}
				} else {
					if(!SaplConstants.log_predicates.contains(tokenBefore)){
						Set<String> acceptable = new HashSet<String>();
						List<String> own = propertiesOfObject.get(concept);
						if(own!=null) acceptable.addAll(own);
						if(cls!=null){
							for(String cl : cls){
								List<String> ofClass = propertiesOfObject.get(cl);
								if(ofClass!=null) acceptable.addAll(ofClass);
							}
						}
						if(supers!=null){
							for(String cl : supers){
								List<String> ofClass = propertiesOfObject.get(cl);
								if(ofClass!=null) acceptable.addAll(ofClass);
							}
						}
						if(!acceptable.isEmpty()){
							if(!acceptable.contains(tokenBefore)){
								if(issues==null) issues = new ArrayList<String>(1);
								if(acceptable.size()==1)
									issues.add(concept+" as an object must be in a statement with "+acceptable.iterator().next()+" as the predicate. \nInstead, '"+tokenBefore+"' is found.");
								else issues.add(concept+" as an object must be in a statement with the predicate that is one of "+acceptable.toString()+". \nInstead, '"+tokenBefore+"' is found.");				
							}
						}
					}
				}
			}
		}
		else {
			if(domain!=null){
				if(tokenBefore.equals(".") || tokenBefore.equals("{")){
					if(issues==null) issues = new ArrayList<String>(2);
					issues.add(concept+" must be used as the predicate in a statement. \nInstead, it is used as the subject.");				
				}
				else if(domain.equals(SaplConstants.Context)){
					if(!tokenBefore.equals("}") && !tokenBefore.equals(";") && !tokenBefore.equals("[") && !tokenBefore.equals("]")
							&& !canTokenBelongToClass(tokenBefore,domain)){
						if(issues==null) issues = new ArrayList<String>(2);
						issues.add(concept+" must be the predicate in a statement with a context container (or a variable) as the subject. \nInstead, '"+tokenBefore+"' is found.");				
					}
				}
				else if(!canTokenBelongToClass(tokenBefore,domain)){
					if(issues==null) issues = new ArrayList<String>(2);
					String rep = report.get(domain);
					issues.add(concept+" must be used as the predicate in a statement with "+(rep!=null?rep:domain)+" as the subject. \nInstead, '"+tokenBefore+"' is found.");
				}
			}
			if(range!=null){
				if(tokenAfter.equals(".") || tokenAfter.equals(";") || tokenAfter.equals(",") || tokenAfter.equals("]") || tokenAfter.equals("}") || tokenAfter.equals("<End of File>")){
					if(issues==null) issues = new ArrayList<String>(2);
					issues.add(concept+" must be used as the predicate in a statement. \nInstead, it is used as the object.");				
				}
				else if(range.equals(SaplConstants.Context)){
					if(!tokenAfter.equals("{") && !canTokenBelongToClass(tokenAfter,range)){
						if(issues==null) issues = new ArrayList<String>(2);
						issues.add(concept+" must be used as the predicate in a statement with a context container (or a variable or s:null) as the object. \nInstead, '"+tokenAfter+"' is found.");
					}
				}
				else if(!canTokenBelongToClass(tokenAfter,range)){
					if(issues==null) issues = new ArrayList<String>(2);
					String rep = report.get(range);
					issues.add(concept+" must be used as the predicate in a statement with "+(rep!=null?rep:range)+" as the object. \nInstead, '"+tokenAfter+"' is found.");
				}
			}
		}
		return issues;
	}

	private static boolean canTokenBelongToClass(String token, String cl) {
		if(token.startsWith(SaplConstants.VARIABLE_PREFIX) || token.equals(SaplConstants.ANYTHING)) return true;
		if(cl.equals(RDFS_RESOURCE)) return true;
		if(cl.equals(EiiConstants.Ontonut)) return true;
		if(token.startsWith(SaplConstants.PARAM_NS)) token = PARAMETER; 
		if(token.equals("{") || token.equals("}") || token.equals(";")) token = SaplConstants.Context;
		
		List<String> supers = superclasses.get(token);
		if(supers!=null)
			if(supers.contains(cl)) return true;
		List<String> cls = individuals.get(token);
		if(cls!=null)
			if(cls.contains(cl)) return true;
		if(cl.equals(BEHAVIOR)){
			if((token.contains(":") && !token.startsWith("\"") && !token.startsWith("'"))
					|| (token.startsWith("<")))
				return true;
			else return false;
		}
		else if(cl.equals(XSD_INTEGER)){
			try {
				if(token.startsWith("\"") && token.endsWith("\"") ||
						token.startsWith("'") && token.endsWith("'"))
					token = token.substring(1,token.length()-1);
				Integer.valueOf(token);
				return true;
			} catch (NumberFormatException e) {
				return false;
			}
		}
		else if(cl.equals(XSD_BOOLEAN)){
			if(token.startsWith("\"") && token.endsWith("\"") ||
					token.startsWith("'") && token.endsWith("'"))
				token = token.substring(1,token.length()-1);
			return token.equals("true") || token.equals("false");
		}
		else if(cl.equals(XSD_STRING)){
			return !(token.equals(SaplConstants.Context));
		}
		else if(cl.equals(SINGLE_PROPERTY_CLASS)){
			if(token.startsWith("<") && token.endsWith(">")) return true;
			if(token.startsWith(SaplConstants.STATEMENT_LOCAL_ID_PREFIX)) return true;
		}
		else if(cl.equals(SaplConstants.Variable)){
			if(token.startsWith(SaplConstants.STATEMENT_LOCAL_ID_PREFIX)) return true;
		}
		else if(cl.equals(SINGLE_PROPERTY_VALUE)) return true;
			
		return false;
	}

	
	/**Any custom-namespace URI or a variable should appear at least twice, 
	 * otherwise likely a typo*/
	private static List<SaplIssue> verifyCustomConceptsAndVars(SaplCode code){
		List<SaplIssue> issues = null;
		
		HashMap<String, String> namespaces = new HashMap<String, String>();
		
		String saplPrefix = "s:";
		String jPrefix = null;
		String pPrefix = null;
		String mPrefix = null;
		String oPrefix = null;
		String dPrefix = null; 
		String rdfPrefix = null; 
		String rdfsPrefix = null; 
		String owlPrefix = null; 
		String xsdPrefix = null;
		
		String textID = code.getTextID();
		List<String> customConcepts = new ArrayList<String>();
		synchronized (globalCustomConcepts) {
			globalCustomConcepts.put(textID, customConcepts);
		}
		
		List<Token> tokens = code.getTokens();

		//Locate heads of the production rules for variables verification
		Stack<Integer> openBrackets = new Stack<Integer>();
		Stack<Integer> queryStart = new Stack<Integer>();
		Stack<Integer> queryEnd = new Stack<Integer>();
		Stack<Integer> queryType = new Stack<Integer>();
		int removeStartedAt = -1;
		int ontonutParamsStartedAt = -1;
		
		for(int i=0; i<tokens.size(); i++){
			Token t = tokens.get(i);
			String token = t.getToken();
			if(oPrefix!=null && token.startsWith(oPrefix))
				token = SaplConstants.ONTONUT_NS+token.substring(oPrefix.length());
			else if(saplPrefix!=null && token.startsWith(saplPrefix) && token.length()>saplPrefix.length())
				token = SaplConstants.SAPL_NS+token.substring(saplPrefix.length());
			
			if(token.equals("{")) openBrackets.push(i);
			else if(token.equals("}")){
				int openBracket = openBrackets.pop();
				if(i<tokens.size()-2){
					String next = getTokenAfter(tokens, i);
					String replace = SaplN3Parser.replace.get(next);
					if(replace!=null) next = replace;
					if(next.startsWith(saplPrefix)) next = SaplConstants.SAPL_NS+next.substring(saplPrefix.length());
					if(	SaplConstants.inference_predicates.contains(next) && tokens.get(i+2).getToken().equals("{")
							|| removeStartedAt==openBracket || ontonutParamsStartedAt==openBracket){
						while(!queryStart.isEmpty() && queryStart.peek()>openBracket){
							queryStart.pop(); // remove any inner queries for rules
							queryEnd.pop();
							queryType.pop();
						}
						queryStart.push(openBracket);
						queryEnd.add(i);
						if(removeStartedAt==openBracket){
							removeStartedAt = -1;
							queryType.add(QUERY_NATURE_REMOVE);
						}
						else if(ontonutParamsStartedAt==openBracket){
							ontonutParamsStartedAt = -1;
							queryType.add(QUERY_NATURE_ONTONUT_PARAMS);
						}
						else{
							queryType.add(QUERY_NATURE_RULE_HEAD);
						}
					}
				}				
			}
			else if(i<tokens.size()-1 && tokens.get(i+1).getToken().equals("{") && 
					( token.equals(SaplConstants.remove) || token.equals(SaplConstants.erase ))){
				removeStartedAt = i+1;
			}
			else if(i<tokens.size()-1 && tokens.get(i+1).getToken().equals("{") && 
					token.equals(EiiConstants.precondition)){
				ontonutParamsStartedAt = i+1;
			}
			if(token.equals(SaplN3Parser.PREFIX_KEYWORD) && i+2<tokens.size()){
				if(tokens.get(i+2).getToken().equals(SAPL_NS_N3)) saplPrefix = tokens.get(i+1).getToken();
				else if(tokens.get(i+2).getToken().equals(PARAM_NS_N3)) pPrefix = tokens.get(i+1).getToken();
				else if(tokens.get(i+2).getToken().equals(RAB_NS_N3)) jPrefix = tokens.get(i+1).getToken();
				else if(tokens.get(i+2).getToken().equals(META_NS_N3)) mPrefix = tokens.get(i+1).getToken();
				else if(tokens.get(i+2).getToken().equals(ONTONUT_NS_N3)) oPrefix = tokens.get(i+1).getToken();
				else if(tokens.get(i+2).getToken().equals(DATA_NS_N3)) dPrefix = tokens.get(i+1).getToken();
				else if(tokens.get(i+2).getToken().equals(RDF_NS_N3)) rdfPrefix = tokens.get(i+1).getToken();
				else if(tokens.get(i+2).getToken().equals(RDFS_NS_N3)) rdfsPrefix = tokens.get(i+1).getToken();
				else if(tokens.get(i+2).getToken().equals(OWL_NS_N3)) owlPrefix = tokens.get(i+1).getToken();
				else if(tokens.get(i+2).getToken().equals(XSD_NS_N3)) xsdPrefix = tokens.get(i+1).getToken();
				else{
					String ns = tokens.get(i+2).getToken();
					ns = ns.substring(1,ns.length()-1);
					namespaces.put(tokens.get(i+1).getToken(), ns);
				}
			}			
		}
		
//		System.out.println(queryStart);//////////////////////
//		System.out.println(queryEnd);//////////////////////
//		System.out.println(queryType);//////////////////////
		
		int contextDepth = 0;
		Stack<Integer> ruleContextDepths = new Stack<Integer>(); //if not empty, we are in the tail of a rule

		HashMap<String, Token> firstConceptOccurancies = new HashMap<String, Token>();
		HashMap<String, String> conceptIsFollowedBy = new HashMap<String, String>();
		HashMap<String, Token> firstVarOccurancies = new HashMap<String, Token>();
		
		Stack<Set<String>> definedVarsForRuleLevels = new Stack<Set<String>>();
		Set<String> definedVars = null; //pointer the the top of the definedVarsForRuleLevels stack
		Set<String> newVarsDefinedAtThisRuleLevel = new HashSet<String>();
		Set<Token> newVarsMustBeDefinedAtThisRuleLevel = new HashSet<Token>();

		boolean insideQuery = false;
		int nextQueryIndex = 0;
		for(int i=0; i<tokens.size(); i++){
			Token t = tokens.get(i);
			String token = t.getToken();
			
			if(token.equals(SaplN3Parser.PREFIX_KEYWORD) && i+2<tokens.size()){
				i = i+2;
				continue;
			}
			
			if(token.equals("{")){
				contextDepth++;
				if(nextQueryIndex<queryStart.size() && queryStart.get(nextQueryIndex)==i) 
					insideQuery = true;
				continue;
			}
			else if(token.equals("}")){
				contextDepth--;
				if(nextQueryIndex<queryStart.size() && queryEnd.get(nextQueryIndex)==i){
					Set<String> allVars = new HashSet<String>(newVarsDefinedAtThisRuleLevel);
					if(definedVars!=null) allVars.addAll(definedVars);
					
					if(queryType.get(nextQueryIndex)!=QUERY_NATURE_ONTONUT_PARAMS)
						newVarsDefinedAtThisRuleLevel.clear();
					
					for(Token varToken : newVarsMustBeDefinedAtThisRuleLevel){
						String var = varToken.getToken();
						if(var.startsWith("%%")) var = SaplConstants.VARIABLE_PREFIX+var.substring(2,var.length()-2);
						if(!allVars.contains(var)){
							//was not defined but was used in an expression, filter, etc
							if(issues==null) issues = new ArrayList<SaplIssue>();
							String text = "Variable '"+var+"' is not defined in the current scope.";
							issues.add(new SaplIssue(text, SaplIssue.SEVERITY_ERROR, code.getTextID(), varToken));
							//toRemove.add(var);
							firstVarOccurancies.remove(var);
						}
					}
					newVarsMustBeDefinedAtThisRuleLevel.clear();
					
					if(queryType.get(nextQueryIndex)==QUERY_NATURE_RULE_HEAD){
						ruleContextDepths.push(contextDepth);
						definedVarsForRuleLevels.push(allVars);
						definedVars = allVars;
						i=i+2;
						contextDepth++;
						//System.out.println("Defined, going in:"+definedVars);
					}
					
					nextQueryIndex++;
					insideQuery = false;
					
				} else if(!ruleContextDepths.isEmpty() && ruleContextDepths.peek()==contextDepth){
					ruleContextDepths.pop();
					Set<String> allVars = definedVarsForRuleLevels.pop();
					allVars.addAll(newVarsDefinedAtThisRuleLevel);
					
					//System.out.println("New:"+newVarsDefinedAtThisRuleLevel);
					//System.out.println("All:"+allVars);
					definedVars = !definedVarsForRuleLevels.isEmpty()? definedVarsForRuleLevels.peek() : null;
					//System.out.println("Defined:"+definedVars);

					for(String var : allVars){
						if(definedVars==null || !definedVars.contains(var)){//gets undefined
							Token first = firstVarOccurancies.get(var);
							if(first.getLine()!=-1){
								if(issues==null) issues = new ArrayList<SaplIssue>(); 
								String text = "Variable '"+var+"' is never used within the scope of the rule.";
								issues.add(new SaplIssue(text, SaplIssue.SEVERITY_WARNING, code.getTextID(), first));
							}
							firstVarOccurancies.remove(var);
						}
					}
					newVarsDefinedAtThisRuleLevel.clear();
				}

				continue;
			}// '}'
			
			if(token.equals(":Print")) continue;
			
			if(token.startsWith("<") && token.endsWith(">")) 
				token = token.substring(1,token.length()-1);
			
			String replace = SaplN3Parser.replace.get(token);
			if(replace!=null) token = replace;
			if(token.startsWith(saplPrefix)) token = SaplConstants.SAPL_NS+token.substring(saplPrefix.length());
			
			if(insideQuery && i<tokens.size()-1 && SaplConstants.embedded_predicates.contains(token)){
				String expression = getTokenAfter(tokens, i);
				int fromExpressionStart = 0;
				if(expression.startsWith("\"") && expression.endsWith("\"")){
					expression = expression.substring(1,expression.length()-1);
					fromExpressionStart++;
				}
				List<String> expressionTokens = ExpressionEvaluator.tokenize(expression);
				
				if(expressionTokens.size()==1){
					String exToken = expressionTokens.get(0);
					if(!exToken.startsWith(SaplConstants.VARIABLE_PREFIX)) continue;
				}
				
				HashSet<String> newVars = new HashSet<String>();
				for(String exToken: expressionTokens){
					if(exToken.startsWith("'") && exToken.endsWith("'") || exToken.startsWith("\"") && exToken.endsWith("\""))
						exToken = exToken.substring(1,exToken.length()-1).trim();
					if(exToken.startsWith(SaplConstants.VARIABLE_PREFIX) && exToken.length()>SaplConstants.VARIABLE_PREFIX.length()){
						if((definedVars==null || !definedVars.contains(exToken)) && !newVarsDefinedAtThisRuleLevel.contains(exToken)){
							Token eT = tokens.get(i+1);
							newVarsMustBeDefinedAtThisRuleLevel.add(new Token(exToken, eT.getLine(), eT.getTextCharStart()+fromExpressionStart, eT.getLineCharStart()+fromExpressionStart));
						}
						Token first = firstVarOccurancies.get(exToken);
						if(first==null){
							Token eT = tokens.get(i+1);
							firstVarOccurancies.put(exToken,new Token(exToken, eT.getLine(), eT.getTextCharStart()+fromExpressionStart, eT.getLineCharStart()+fromExpressionStart));
							newVars.add(exToken);
						}
						else if(first.getLine()!=-1 && !newVars.contains(exToken)) 
							firstVarOccurancies.put(exToken,new Token(null, -1, -1, -1));
					}
					fromExpressionStart+=exToken.length();
				}
				i++;
				continue;
			}
			else if(insideQuery && i<tokens.size()-1 && SaplConstants.set_predicates.contains(token)){
				String exToken = getTokenAfter(tokens, i);
				if(exToken.startsWith(SaplConstants.VARIABLE_PREFIX)){
					if((definedVars==null || !definedVars.contains(exToken)) && !newVarsDefinedAtThisRuleLevel.contains(exToken)){
						Token eT = tokens.get(i+1);
						newVarsMustBeDefinedAtThisRuleLevel.add(eT);
					}
					Token first = firstVarOccurancies.get(exToken);
					if(first==null){
						Token eT = tokens.get(i+1);
						firstVarOccurancies.put(exToken,eT);
					}
					else if(first.getLine()!=-1) 
						firstVarOccurancies.put(exToken,new Token(null, -1, -1, -1));
				}
				i++;
				continue;
			}
			
			if(token.startsWith(SaplConstants.SAPL_NS)) continue;
			
			if(jPrefix!=null && token.startsWith(jPrefix) || token.startsWith(SaplConstants.RAB_NS)) continue;
			if(pPrefix!=null && token.startsWith(pPrefix)|| token.startsWith(SaplConstants.PARAM_NS)) continue;
			if(mPrefix!=null && token.startsWith(mPrefix) || token.startsWith(SaplConstants.META_NS)) continue;
			if(dPrefix!=null && token.startsWith(dPrefix) || token.startsWith(SaplConstants.DATA_NS)) continue;
			if(rdfPrefix!=null && token.startsWith(rdfPrefix) || token.startsWith(SaplConstants.RDF_NS)) continue;
			if(rdfsPrefix!=null && token.startsWith(rdfsPrefix) || token.startsWith(SaplConstants.RDFS_NS)) continue;
			if(owlPrefix!=null && token.startsWith(owlPrefix) || token.startsWith(SaplConstants.OWL_NS)) continue;
			if(xsdPrefix!=null && token.startsWith(xsdPrefix) || token.startsWith(SaplConstants.XSD_NS)) continue;

			if(oPrefix!=null && token.startsWith(oPrefix) || token.startsWith(SaplConstants.ONTONUT_NS)){
				if(i<tokens.size()-1){
					if(oPrefix!=null && token.startsWith(oPrefix))
						token = SaplConstants.ONTONUT_NS+token.substring(oPrefix.length());
					if(token.equals(EiiConstants.input)){
						do { 
							i++;
							String varToken = tokens.get(i).getToken();
							newVarsDefinedAtThisRuleLevel.add(varToken);
							Token first = firstVarOccurancies.get(varToken);
							if(first==null){
								firstVarOccurancies.put(varToken,tokens.get(i));
							}
							else if(first.getLine()!=-1) firstVarOccurancies.put(varToken,new Token(null, -1, -1, -1));
						} while(tokens.get(i+1).getToken().equals(",") && (i++ < tokens.size()-1));
					}
					else if(token.equals(EiiConstants.uri) || token.equals(EiiConstants.data) || token.equals(EiiConstants.body)){
						String varToken = tokens.get(i+1).getToken();
						int ind=0;
						HashSet<String> newVars = new HashSet<String>();
						while(true){
							ind = varToken.indexOf("%%", ind);
							if(ind!=-1){
								int ind2 = varToken.indexOf("%%", ind+3);
								if(ind2!=-1){
									String value = SaplConstants.VARIABLE_PREFIX+varToken.substring(ind+2, ind2);
									
									if((definedVars==null || !definedVars.contains(value)) && !newVarsDefinedAtThisRuleLevel.contains(value)){
										Token eT = tokens.get(i+1);
										newVarsMustBeDefinedAtThisRuleLevel.add(new Token(varToken.substring(ind, ind2+2), eT.getLine(), eT.getTextCharStart()+ind, eT.getLineCharStart()+ind));
									}
									Token first = firstVarOccurancies.get(value);
									if(first==null){
										Token eT = tokens.get(i+1);
										firstVarOccurancies.put(value, new Token(varToken.substring(ind, ind2+2), eT.getLine(), eT.getTextCharStart()+ind, eT.getLineCharStart()+ind));
										newVars.add(value);
									}
									else if(first.getLine()!=-1 && !newVars.contains(value)) 
										firstVarOccurancies.put(value,new Token(null, -1, -1, -1));
									
									ind = ind2+2;
								}
								else break;
							}
							else break;
						}	
								
					}
				}
				continue;
			}

			if(token.startsWith("\"") || token.startsWith("'")){
				for(String seenToken : firstVarOccurancies.keySet()){
					Token first = firstVarOccurancies.get(seenToken);
					if(first.getLine()!=-1 && seenToken.startsWith(SaplConstants.VARIABLE_PREFIX)){
						if(token.contains(seenToken))
							firstVarOccurancies.put(seenToken,new Token(null, -1, -1, -1));
					}
				}
				continue;
			}
			
			if(token.startsWith(SaplConstants.VARIABLE_PREFIX) && token.length()>SaplConstants.VARIABLE_PREFIX.length()){
				if(!insideQuery){
					if(definedVars!=null && !definedVars.contains(token) && !SaplCodeVerifier.ignoreVars.contains(token)){
						if(issues==null) issues = new ArrayList<SaplIssue>(); 
						String text = "Variable '"+token+"' is not defined in the current scope.";
						issues.add(new SaplIssue(text, SaplIssue.SEVERITY_ERROR, code.getTextID(), t));
						continue;
					}
				}
				else{
					String next = getTokenAfter(tokens, i);
					String nextReplace = SaplN3Parser.replace.get(next);
					if(nextReplace!=null) next = nextReplace;
					if(next.startsWith(saplPrefix)) next = SaplConstants.SAPL_NS+next.substring(saplPrefix.length());
					
					if(!SaplConstants.log_predicates.contains(next)){
						if(definedVars==null || !definedVars.contains(token))
							newVarsDefinedAtThisRuleLevel.add(token);
					}
					else{
						if((definedVars==null || !definedVars.contains(token)) && !newVarsDefinedAtThisRuleLevel.contains(token))
							newVarsMustBeDefinedAtThisRuleLevel.add(t);
					}
				}
				Token first = firstVarOccurancies.get(token);
				if(first==null){
					if(insideQuery) firstVarOccurancies.put(token,t);
				}
				else if(first.getLine()!=-1) firstVarOccurancies.put(token,new Token(null, -1, -1, -1));
				
			}
			else if(token.length()>2 && token.contains(":") && !token.endsWith(":") && !token.startsWith("http:") && !token.startsWith("https:")){
				int ind = token.indexOf(":");
				String ns = namespaces.get(token.substring(0,ind+1));
				if(ns!=null)
					token = ns+token.substring(ind+1);
				Token first = firstConceptOccurancies.get(token);
				if(first==null){
					firstConceptOccurancies.put(token,t);
					String next = i==tokens.size()-1 ? "" : getTokenAfter(tokens, i);
					conceptIsFollowedBy.put(token, next);
					synchronized (globalCustomConcepts) {
						customConcepts.add(token);
					}
				}
				else if(first.getLine()!=-1) firstConceptOccurancies.put(token,new Token(null, -1, -1, -1));
			}
		}//for each token
		
		for(String token : firstConceptOccurancies.keySet()){
			Token first = firstConceptOccurancies.get(token);
			if(first.getLine()!=-1){
				String nextWas = conceptIsFollowedBy.get(token);
				if(!nextWas.equals("a") && !nextWas.equals("rdf:type")){
					boolean found = false;
					synchronized (globalCustomConcepts) {
						for(List<String> otherCustomConcepts : globalCustomConcepts.values()){
							if(otherCustomConcepts!=customConcepts)
								if(otherCustomConcepts.contains(token)){
									found = true;
									break;
								}
						}	
					}
					if(!found){
						if(issues==null) issues = new ArrayList<SaplIssue>(); 
						String text = "Custom concept '"+token+"' appears only one time in the project: may be a typo";
						issues.add(new SaplIssue(text, SaplIssue.SEVERITY_WARNING, code.getTextID(), first));
					}
				}
			}
		}

		return issues;
	}
	
	static HashSet<String> ignoreVars = SaplCodeVerifier.createIgnoreVars();
	static HashSet<String> createIgnoreVars(){
		HashSet<String> result =  new HashSet<String>(8);
		result.add(SaplConstants.VARIABLE_PREFIX+"result");
		result.add(SaplConstants.VARIABLE_PREFIX+"return");
		result.add(SaplConstants.VARIABLE_PREFIX+"ontonutErrorSource");
		result.add(SaplConstants.VARIABLE_PREFIX+"ontonutErrorMessage");
		result.add(SaplConstants.VARIABLE_PREFIX+"ontonutErrorTrace");
		return result;
	};
	
	/**Access S-APL ontology to obtain definitions of concepts*/

	static final Map<String,String> domains = new HashMap<String,String>();
	static final Map<String,String> ranges = new HashMap<String,String>();
	static final Map<String,List<String>> propertiesOfSubject = new HashMap<String,List<String>>();
	static final Map<String,List<String>> propertiesOfObject = new HashMap<String,List<String>>();
	static final Set<String> classes = new HashSet<String>();
	static final Map<String,List<String>> superclasses = new HashMap<String,List<String>>();
	static final Map<String,List<String>> individuals = new HashMap<String,List<String>>();
	
	static {
		accessSaplOntology();
	}
	
	static void accessSaplOntology() {
		Bundle b = Platform.getBundle("SAPL2");
		Enumeration<URL> entries = b.findEntries("ontology", "*.sapl", true);
		if(entries!=null){
			while (entries.hasMoreElements()){
				URL url = entries.nextElement(); 
				String filename = url.toString();
				filename = filename.substring(filename.indexOf("ontology"));
				try {
				    InputStream inputStream = url.openConnection().getInputStream();
				    BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
				    String content="";
				    String inputLine;
				    while ((inputLine = in.readLine()) != null) {
				    	content += inputLine+"\n";
				    }
				    in.close();
				    
				    SaplCode code = SaplN3Parser.compile(content, filename);
					SaplModel model = new SaplModel(false, false);
					SaplN3Parser.load(code.getCode(), model, SaplConstants.GENERAL_CONTEXT, null, filename, false);

					SaplQueryResultSet rs = new SaplQueryResultSet();
					SaplQueryEngine.evaluateQueryN3("?concept <http://www.w3.org/2000/01/rdf-schema#domain> ?domain; <http://www.w3.org/2000/01/rdf-schema#range> ?range", SaplConstants.GENERAL_CONTEXT, model, false, rs);
					for(int i=0; i<rs.getNumberOfSolutions();i++){
						Map<String, String> sol = rs.getSolution(i);
						String concept = sol.get("concept");
						String domain = sol.get("domain");
						domains.put(concept,  domain);
						String range = sol.get("range");
						ranges.put(concept,  range);
						if(!domain.equals(SaplConstants.Context) && !domain.equals(SaplConstants.Variable)
								&& !domain.equals(RDFS_RESOURCE)){
							List<String> props = propertiesOfSubject.get(domain);
							if(props==null){
								props = new ArrayList<String>(2);
								propertiesOfSubject.put(domain, props);
							}
							props.add(concept);
						}
						if(!range.equals(SaplConstants.Context) && !range.equals(SaplConstants.Variable)
								&& !range.equals(RDFS_RESOURCE)){
							List<String> props = propertiesOfObject.get(range);
							if(props==null){
								props = new ArrayList<String>(2);
								propertiesOfObject.put(range, props);
							}
							props.add(concept);
						}
					}
					
					rs = new SaplQueryResultSet();
					SaplQueryEngine.evaluateQueryN3("?concept a <http://www.w3.org/2000/01/rdf-schema#Class>. {?concept <http://www.w3.org/2000/01/rdf-schema#subClassOf> ?super} <"+SaplConstants.SAPL_NS+"is> <"+SaplConstants.SAPL_NS+"Optional>", SaplConstants.GENERAL_CONTEXT, model, false, rs);
					for(int i=0; i<rs.getNumberOfSolutions();i++){
						Map<String, String> sol = rs.getSolution(i);
						String concept = sol.get("concept");
						classes.add(concept);
						String superConcept = sol.get("super");
						if(superConcept!=null){
							List<String> supers = superclasses.get(concept);
							if(supers==null){
								supers = new ArrayList<String>(2);
								superclasses.put(concept, supers);
							}
							supers.add(superConcept);
							String rep = report.get(superConcept);
							if(rep==null) rep = "";
							String conceptRep = report.get(concept);
							if(conceptRep==null) conceptRep = concept.replace(SaplConstants.SAPL_NS, "s:");
							rep += (rep.isEmpty()?"":", ")+conceptRep;
							report.put(superConcept, rep);
						}
					}

					rs = new SaplQueryResultSet();
					SaplQueryEngine.evaluateQueryN3("?concept a ?class", SaplConstants.GENERAL_CONTEXT, model, false, rs);
					for(int i=0; i<rs.getNumberOfSolutions();i++){
						Map<String, String> sol = rs.getSolution(i);
						String concept = sol.get("concept");
						String cl = sol.get("class");
						if(cl.equals(RDFS_CLASS) || cl.equals(RDF_PROPERTY) || cl.equals(FUNCTION))
							continue;
						List<String> cls = individuals.get(concept);
						if(cls==null){
							cls = new ArrayList<String>(2);
							individuals.put(concept, cls);
						}
						cls.add(cl);
						String rep = report.get(cl);
						if(rep==null) rep = "";
						String conceptRep = report.get(concept);
						if(conceptRep==null) conceptRep = concept.replace(SaplConstants.SAPL_NS, "s:");
						rep += (rep.isEmpty()?"":", ")+conceptRep;
						report.put(cl, rep);
					}
					
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}	
	
}
