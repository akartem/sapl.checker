package sapl.checker.launcher;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Platform;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.ui.ILaunchShortcut;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.osgi.framework.Bundle;

/**
 * @author Artem Katasonov (VTT)
 * 
 * @version 1.1 (25.05.2015)
 * 
 * Copyright (C) 2014-2015, VTT
 * 
 * */

public class SaplLauncher implements ILaunchShortcut {

	@Override
	public void launch(ISelection selection, String mode) {
		if (selection instanceof IStructuredSelection) {
			Object[] ss = ((IStructuredSelection)selection).toArray();
			ArrayList<String> scripts = new ArrayList<String>();
			IProject prj = null;
			for(Object obj : ss){
				if(obj instanceof IFile){
					IFile file = (IFile)obj;
					prj = file.getProject();
					scripts.add(file.getLocation().toString());
				}
			}

			searchAndLaunch(prj, scripts.toArray(new String[scripts.size()]), mode);
		} 
	}

	@Override
	public void launch(IEditorPart editor, String mode) {
		IEditorInput input = editor.getEditorInput();
		IFile file = (IFile) input.getAdapter(IFile.class);
		
		if (file != null) {
			String[] scripts = new String[1];
			scripts[0] = file.getLocation().toString();
			IProject prj = file.getProject();
			
			searchAndLaunch(prj, scripts, mode);
		}
	}


	private void searchAndLaunch(IProject project, String[] scripts, String mode) {
		
		String name = scripts[0].substring(scripts[0].lastIndexOf('/')+1); 

		String args = "-name "+name+" ";
		if(mode.equals("debug")) args += "-eiidebug -printdb ";
		else args += "-noprint ";
		for(String script : scripts){
			args += script+" ";
		}
		args = args.trim();
		
		//System.out.println("Launching "+args);
		
		try {
			ILaunchManager lm = DebugPlugin.getDefault().getLaunchManager();
			ILaunchConfigurationType configType = lm.getLaunchConfigurationType("org.eclipse.jdt.launching.localJavaApplication");
			//ILaunchConfigurationType configType = lm.getLaunchConfigurationType("sapl.checker.localSaplApplication");
			ILaunchConfigurationWorkingCopy wc = configType.newInstance(null, name); 
			wc.setAttribute("org.eclipse.jdt.launching.PROJECT_ATTR", project.getName());
			wc.setAttribute("org.eclipse.jdt.launching.MAIN_TYPE", "sapl.Sapl");
			wc.setAttribute("org.eclipse.jdt.launching.DEFAULT_CLASSPATH", false);
			wc.setAttribute("org.eclipse.jdt.debug.ui.INCLUDE_EXTERNAL_JARS", true);
			wc.setAttribute("org.eclipse.jdt.launching.ATTR_USE_START_ON_FIRST_THREAD", true);
			
			List<String> l = new ArrayList<String>();
			l.add("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><runtimeClasspathEntry containerPath=\"org.eclipse.jdt.launching.JRE_CONTAINER\" path=\"1\" type=\"4\"/>");
			for(String lib : classpathEntries){
				l.add("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><runtimeClasspathEntry externalArchive=\""+lib+"\" path=\"3\" type=\"2\"/>");
			}
			
			IFolder binFolder = project.getFolder("bin");
			if(binFolder!=null){ 
				l.add("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><runtimeClasspathEntry internalArchive=\""+binFolder.getFullPath().toString()+"\" path=\"3\" type=\"2\"/>");
			}
			wc.setAttribute("org.eclipse.jdt.launching.CLASSPATH", l);

			wc.setAttribute("org.eclipse.jdt.launching.PROGRAM_ARGUMENTS", args);
			
			ILaunchConfiguration config = wc.doSave();
			config.launch(mode, null);
			
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}
	
	static final List<String> classpathEntries = SaplLauncher.createClasspathEntries();
	static List<String> createClasspathEntries() {

		Bundle b = Platform.getBundle("SAPL2");
		String id = String.valueOf(b.getBundleId());
		
		String eclipseHome = System.getProperty("eclipse.home.location");
		eclipseHome = eclipseHome.substring(eclipseHome.indexOf("file:")+6);
		
		String bundleLocation = b.getLocation();
		
		int ind = bundleLocation.indexOf(eclipseHome);
		if(ind!=-1) bundleLocation = bundleLocation.substring(ind);
		else{
			int ind2 = bundleLocation.indexOf("dropins");
			if(ind2==-1) bundleLocation.indexOf("plugins");
			if(ind2!=-1)
				bundleLocation = eclipseHome + bundleLocation.substring(ind2);
			else bundleLocation = bundleLocation.substring(bundleLocation.indexOf("file:")+6);
		}
		
		ArrayList<String> words = new ArrayList<String>();
		Enumeration<URL> entries = b.findEntries("", "*.jar", true);
		if(entries!=null){
			while (entries.hasMoreElements()){
				URL url = entries.nextElement(); 
				String name = url.toString();
				int ind3 = name.indexOf(id);
				name = bundleLocation+name.substring(name.indexOf('/', ind3)+1);
				words.add(name);
			}
		}
		entries = b.findEntries("", "bin", true);
		if(entries!=null){
			while (entries.hasMoreElements()){
				URL url = entries.nextElement(); 
				String name = url.toString();
				int ind3 = name.indexOf(id);
				name = bundleLocation+name.substring(name.indexOf('/', ind3)+1);
				words.add(name);
			}
		}
		
		return Collections.unmodifiableList(words);
	}

}
